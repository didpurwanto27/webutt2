<section id="content">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<article>
							<div class="post-image">
								<div class="post-heading">
									<h4><a href="#">PENUNDAAN HASIL UJIAN DAN PERPANJANGAN PENDAFTARAN UJUL 2018.2</a></h4>
								</div>
							</div>
							<div class="alert alert-info">

								<b> PENTING </b> <br />

								Setiap mahasiswa yang akan registrasi ujian ulang, harus melakukan registrasi mahasiswa lama terlebih dahulu dengan cara <a class="btn btn-success" href="http://ut-taiwan.org/registrasilama/"> Klik Disini </a>	<br />

							</div>
							<br>
							<div class="alert alert-info">

								<font color="red"><b> PENGUMUMAN PENTING! </b> </font><br />
								Pengumuman hasil Ujian Akhir Semester (UAS) Program Sarjana (kecuali PGSD dan PGPAUD), Diploma, dan Pascasarjana semester 2017/18.2 yang seharusnya diumumkan pada <font color="red">Jumat, 6 Juli 2018</font>, ditunda menjadi <font color="red">Jum'at, 20 Juli 2018 </font>. Maka dari itu batas pengajuan ujian ulang diperpanjang dari tanggal <font color="red">15 Juli 2018</font> menjadi sampai dengan tanggal <font color="red">23 Juli 2015</font>. mohon diperhatikan bagi mahasiswa yang belum registrasi online harus melakukan registrasi terlebih dahulu untuk melakukan registrasi ujian ulang paling lambat <font color="red">10 Juli 2018</font>.
								
								
							</div>
							<br>
							
							<p>	
								<b>Alur Daftar ulang dan pengajuan Ujian Ulang :</b><br />
								
								<ol>
									<li>Mahasiswa Lama melakukan registrasi secara online melalui
									<a class="btn btn-success" href="http://ut-taiwan.org/registrasilama/"> Klik Disini </a>
									
									</li>
									<br />
									<li>Jika ada matakuliah yg ingin diulang bisa melakukan pengajuan ujian ulang melalui <a class="btn btn-success" href="http://ut-taiwan.org/ujianulangan/"> Klik Disini </a>
									</li>
									<br />
									<li>Setelah proses 1 dan 2 selesai, mahasiswa diwajibkan untuk melunasi uang kuliah (UK) NT$ 4500 / IDR 1.750.000 dan uang operasional (UO) NT$ 2000 atau IDR 946.000. Transfer UK dan UO dapat dijadikan satu (NT$ 6500 atau IDR 2.696.000).
									<table border="1">
								<tbody>
								<tr>
								<td rowspan="2" width="119">
								<p align="center"><strong>PEMBAYARAN</strong></p>
								</td>
								<td colspan="2" width="331">
								<p class="MsoNormal" style="mso-margin-top-alt: auto; margin-right: 6.0pt; mso-margin-bottom-alt: auto; margin-left: 6.0pt; text-align: center; line-height: normal;" align="center"><strong><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">JUMLAH YANG DIBAYARKAN</span></strong></p>
								</td>
								</tr>
								<tr>
								<td width="141">
								<p class="MsoNormal" style="mso-margin-top-alt: auto; margin-right: 6.0pt; mso-margin-bottom-alt: auto; margin-left: 6.0pt; text-align: center; line-height: normal;" align="center"><strong><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">UANG KULIAH</span></strong></p>
								</td>
								<td style="width: 142.5pt; padding: 0in 0in 0in 0in;" width="190">
								<p class="MsoNormal" style="mso-margin-top-alt: auto; margin-right: 6.0pt; mso-margin-bottom-alt: auto; margin-left: 6.0pt; text-align: center; line-height: normal;" align="center"><strong><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">UANG OPERASIONAL</span></strong></p>
								</td>
								</tr>
								<tr>
								<td style="width: 89.25pt; padding: 0in 0in 0in 0in;" valign="top" width="119">
								<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Taiwan</span></p>
								</td>
								<td style="width: 105.75pt; padding: 0in 0in 0in 0in;" valign="top" width="141">
								<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">4500 NT</span></p>
								</td>
								<td style="width: 142.5pt; padding: 0in 0in 0in 0in;" valign="top" width="190">
								<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">2000 NT</span></p>
								</td>
								</tr>
								<tr>
								<td style="width: 89.25pt; padding: 0in 0in 0in 0in;" valign="top" width="119">
								<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Indonesia</span></p>
								</td>
								<td style="width: 105.75pt; padding: 0in 0in 0in 0in;" valign="top" width="141">
								<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Rp 1.750.000 </span></p>
								</td>
								<td style="width: 142.5pt; padding: 0in 0in 0in 0in;" valign="top" width="190">
								<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Rp 946.000</span></p>
								</td>
								</tr>
								</tbody>
								</table>
								</br>
								<font color = "red"><strong><u>** Transfer UK dan UO dapat dijadikan satu (NT$ 6500 atau IDR 2.696.000).</u></strong></font>
									</li>
									<br />
									<li>Jika ada mata kuliah yang diulang, diwajibkan melunasi Uang Ujian Ulang, sesuai dgn jumlah mata kuliah yg diulang. <u>( NT$ 300 atau IDR 120.000 per SKS)</u>
									</li>
									<br />
									<li>Uang pembayaran dapat ditransfer ke rekening berikut: <br />
						<font color='green'><b><u>Bank Taiwan:</u></b></font>
						  <table>
							<tr>
								<td> Bank </td>
								<td> <b>: Hua Nan Bank  </b>
							<tr>						  
							<tr>
								<td> Atas nama </td>
								<td> <b>: Ariana Tulus Purnomo  </b>
							<tr>
							
							<tr>
								<td> Bank Code </td>
								<td> <b>: 008</b>
							<tr>
							<tr>
								<td> Swift Code </td>
								<td> <b>: HNBKTWTP</b>
							<tr>
							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Taita (National Taiwan University) </b>
							<tr>							
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 154-20-047668-7 </b>
							<tr>
							<tr>
								<td> No Telp. </td>
								<td> <b>:  0974-086-541 </b>
							<tr>							
						  </table>
						  
						  <br />
						  <font color='green'><b><u>Atau di Bank Indonesia:</u></b></font>
						  <table>
							<tr>
								<td> Bank </td>
								<td> <b>: Bank BNI  </b>
							<tr>
							<tr>
								<td> Atas Nama </td>
								<td> <b>: Ervin Kusuma Dewi </b>
							<tr>
							
							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Banyuwangi</b>
							<tr>
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 0074228645 </b>
							<tr>
							<tr>
								<td> No. Telp </td>
								<td> <b>: 0963-504-467 </b>
							<tr>
						  </table>
									</li>
									<br />

									<li>Setelah melakukan pembayaran, mahasiswa DIHARUSKAN untuk melakukan konfirmasi pembayaran melalui dengan klik <a href="http://ut-taiwan.org/cp17/index.php/pages/konfirmasi_bayar" class="btn btn-success">KONFIRMASI </a> dengan mengunggah foto bukti pembayaran (bukan foto selfie atau selca).
									</li>
									<br />
									<li>Mahasiswa lama yang membayar biaya registrasi ulang dan UJUL diluar batas tanggal yang ditentukan maka mahasiswa ybs tidak akan teregistrasi semester ini (2018.2/19.1) dan dianggap <font color="red"><b>CUTI</b></font>.
									</li>
									<br />
									<li>Apabila ada pertanyaan terkait rincian biaya, cara pembayaran, dan masalah keuangan lainya bisa add line bendahara: <p class="btn btn-danger" >@tvt3764t <p>
									</li>								
									<li>Pertanyaan terkait administrasi, silakan menghubungi Facebook UT Taiwan melalui <font color="blue">www.facebook.com/ut.taiwan </font> atau bisa add line Admin: <p class="btn btn-danger" >@adp1367m <p>
									</li>

								</ol>
								<br /><br />
								Terima kasih<br />
								Salam UT Taiwan<br />
								Mandiri Mengukir Prestasi<br />
							</p>
						</article>
						
					</div>
					</div>
				</div>
			</div>
		</section>