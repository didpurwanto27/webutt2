<body>

<div class="colorlib-loader"></div>

<div id="page">
<nav class="colorlib-nav" role="navigation">
	
	<div class="top-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<div id="colorlib-logo"><a href="<?php echo base_url('index.php/pages/')?>/index">UT Taiwan</a></div>
				</div>
				<div class="col-md-10 text-right menu-1">
					<ul>
						<li class="<?php if($currentPage =='index'){echo 'active';}?>"><a href="index">Beranda</a></li>
						<!-- <li><a href="index">Beranda</a></li> -->
						<li class="has-dropdown <?php if($currentPage =='pembayaran'){echo 'active';}?>">
							<a href="#">Pembayaran</a>
							<ul class="dropdown">
								<li class="<?php if($currentMenu =='cara_bayar'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/cara_bayar">Cara Pembayaran</a></li>
								<li class="<?php if($currentMenu =='konfirmasi_bayar'){echo 'active';}?>"><a href="http://ut-taiwan.org/cp17/index.php/pembayaran/konf_page">Konfirmasi Pembayaran</a></li>
								<li class="<?php if($currentMenu =='status_bayar'){echo 'active';}?>"><a href="http://ut-taiwan.org/cp17/index.php/pembayaran/status_pembayaran">Status Pembayaran</a></li>
								<li class="<?php if($currentMenu =='biaya'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/biaya">Biaya Pendidikan</a></li>
							</ul>
						</li>
						<li class="has-dropdown <?php if($currentPage =='registrasi'){echo 'active';}?>">
							<a href="#">Registrasi</a>
							<ul class="dropdown">
								<li class="<?php if($currentMenu =='regis_maba'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Pendaftaran Mahasiswa Baru</a></li>
								<li class="<?php if($currentMenu =='regis_mala'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></li>
								<li class="<?php if($currentMenu =='ujul'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/ujul">Ujian Ulang</a></li>
								<li class="<?php if($currentMenu =='lokasi'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/lokasi">Lokasi Pendaftaran</a></li>
							</ul>
						</li>
						<li class="has-dropdown <?php if($currentPage =='akademik'){echo 'active';}?>">
							<a href="#">Akademik</a>
							<ul class="dropdown">
								<li class="<?php if($currentMenu =='program_studi'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/program_studi">Program Studi</a></li>
								<li class="<?php if($currentMenu =='mekanisme_kuliah'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/mekanisme_kuliah">Mekanisme Perkuliahan</a></li>
								<li class="<?php if($currentMenu =='link_akademik'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/link_akademik">Link Akademik</a></li>
							</ul>
						</li>
						<li class="has-dropdown <?php if($currentPage =='qna'){echo 'active';}?>">
							<a href="#">Tanya Jawab</a>
							<ul class="dropdown">
								<li class="<?php if($currentMenu =='umum'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/umum">Umum</a></li>
								<li class="<?php if($currentMenu =='perkuliahan'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/perkuliahan">Perkuliahan</a></li>
							</ul>
						</li>
						<li class="has-dropdown <?php if($currentPage =='tentang'){echo 'active';}?>">
							<a href="#">Tentang</a>
							<ul class="dropdown">
								<li class="<?php if($currentMenu =='sekilas'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/sekilas">Sekilas</a></li>
								<li class="<?php if($currentMenu =='sejarah'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/sejarah">Sejarah</a></li>
								<li><a href="<?php echo base_url('index.php/pages/')?>/bapel">Badan Pelaksana</a></li>
							</ul>
						</li>
						<!--li class="<?php if($currentPage =='berita'){echo 'active';}?>"><a href="<?php echo base_url('index.php/pages/')?>/berita">Berita</a></li-->
					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>