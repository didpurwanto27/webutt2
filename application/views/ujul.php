<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Ujian Ulang</a></h3>
						</div>
					</div>
					<div class="alert alert-info">

						<b> PENTING </b> <br />

						Setiap mahasiswa yang akan registrasi ujian ulang, harus melakukan registrasi mahasiswa lama terlebih dahulu dengan cara <a class="btn btn-success"  href="<?php echo base_url('index.php/pages/')?>/regis_mala"> Klik Disini </a>	<br />

					</div>
					<br>
					<!--div class="alert alert-info">

						<font color="red"><b> PENGUMUMAN PENTING! </b> </font><br />
						Pengumuman hasil Ujian Akhir Semester (UAS) Program Sarjana (kecuali PGSD dan PGPAUD), Diploma, dan Pascasarjana semester <font color="brown"><span style="background-color: #FFFF00">2017/18.2</span></font> yang seharusnya diumumkan pada <font color="brown"><span style="background-color: #FFFF00">Jumat, 6 Juli 2018</span></font>, ditunda menjadi <font color="brown"><span style="background-color: #FFFF00">Jum'at, 20 Juli 2018 </span></font>. Maka dari itu batas pengajuan ujian ulang diperpanjang dari tanggal <font color="brown"><span style="background-color: #FFFF00">15 Juli 2018</span></font> menjadi sampai dengan tanggal <font color="brown"><span style="background-color: #FFFF00">23 Juli 2015</span></font>. mohon diperhatikan bagi mahasiswa yang belum registrasi online harus melakukan registrasi terlebih dahulu untuk melakukan registrasi ujian ulang paling lambat <font color="brown"><span style="background-color: #FFFF00">10 Juli 2018</span></font>.												
					</div-->
					<br>
					
					<p>	
						<b>Alur Daftar ulang dan pengajuan Ujian Ulang:</b><br />
						
						<ol>
							<li>Mahasiswa Lama melakukan registrasi secara online melalui
							<a class="btn btn-success" href="http://ut-taiwan.org/registrasilama/"> Klik Disini </a>
							
							</li>
							<br />
							<li>Jika ada matakuliah yg ingin diulang bisa melakukan pengajuan ujian ulang melalui <a class="btn btn-success" href="http://ut-taiwan.org/ujianulangan/"> Klik Disini </a>
							</li>
							<br />
							<li>Setelah proses 1 dan 2 selesai, mahasiswa diwajibkan untuk melunasi uang kuliah (UK)
							 <font color="brown"> <span style="background-color: #FFFF00">NTD 4.500 atau IDR  1.750.000 </span> </font> dan uang operasional (UO)
							 <font color="brown">  <span style="background-color: #FFFF00">NTD 2.000 atau Rp 970.000,00</span></font>.


							 Transfer UK dan UO dapat dijadikan satu <font color="brown">  <span style="background-color: #FFFF00"> NT$ 6.500 atau IDR 2.720.000 </span></font>.
							</font>
							</li>
							<br />
							<li>Jika ada mata kuliah yang diulang, diwajibkan melunasi Uang Ujian Ulang, sesuai dgn jumlah mata kuliah yg diulang. <font color="brown">  <span style="background-color: #FFFF00">NT$ 300 atau IDR 120.000 per SKS</span></font>
							</li>
							<br />
							<li>Uang pembayaran dapat ditransfer ke rekening berikut: <br />
								<font color='green'><b><u>Bank Taiwan:</u></b></font>
							<table>
							<tr>
								<td> Bank </td>
								<td> <b>: Hua Nan Bank  </b>
							<tr>						  
							<tr>
								<td> Atas nama </td>
								<td> <b>: Ariana Tulus Purnomo  </b>
							<tr>

							<tr>
								<td> Bank Code </td>
								<td> <b>: 008</b>
							<tr>
							<tr>
								<td> Swift Code </td>
								<td> <b>: HNBKTWTP</b>
							<tr>
							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Taita (National Taiwan University) </b>
							<tr>							
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 154-20-047668-7 </b>
							<tr>
							<tr>
								<td> No Telp. </td>
								<td> <b>:  +886-974-086-541 </b>
							<tr>							
							</table>

							<br />
							<font color='green'><b><u>Atau di Bank Indonesia:</u></b></font>
							<table>
							<tr>
								<td> Bank </td>
								<td> <b>: Bank BNI  </b>
							<tr>
							<tr>
								<td> Atas Nama </td>
								<td> <b>: Hana Mutialif Maulidiah </b>
							<tr>

							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Tanjung Perak</b>
							<tr>
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 0296233345 </b>
							<tr>
							<tr>
								<td> No. Telp </td>
								<td> <b>: +886-973-453-283 </b>
							<tr>
							</table>
							</li>
							<br />

							<li>Setelah melakukan pembayaran, mahasiswa DIHARUSKAN untuk melakukan konfirmasi pembayaran melalui dengan klik <a href="http://ut-taiwan.org/cp17/index.php/pembayaran/konf_page" class="btn btn-success">KONFIRMASI </a> dengan mengunggah foto bukti pembayaran (bukan foto selfie atau selca).
							</li>
							<br />
							<li>Mahasiswa lama yang membayar biaya registrasi ulang dan UJUL diluar batas tanggal yang ditentukan maka mahasiswa ybs tidak akan teregistrasi semester ini (2018.2/19.1) dan dianggap <font color="red"><b>CUTI</b></font>.
							</li>
							<br />
							<li>Apabila ada pertanyaan terkait rincian biaya, cara pembayaran, dan masalah keuangan lainya bisa add line bendahara: <span style="background-color: #ffffee">@tvt3764t </span>
							</li>								
							<li>Pertanyaan terkait administrasi, silakan menghubungi Facebook UT Taiwan melalui <font color="blue"><b><a href="https://facebook.com/ut.taiwan">www.facebook.com/ut.taiwan</a></b> </font> atau bisa add line Admin: <span style="background-color: #ffffee">@adp1367m </span>
							</li>

						</ol>
						
					</p>
				</article>
				
			</div>
			</div>
		</div>
	</div>
</section>
<br>
<br>