<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3>Mekanisme Perkuliahan</h3>
						</div>
					</div>
					
					
					<p><strong>Sistem Pembelajaran</strong></p>
					<p>UT memiliki sistem pembelajaran jarak jauh dan terbuka. Jarak jauh berarti pembelajaran tidak dilakukan secara langsung melainkan melalui media berupa cetak (buku modul) maupun non cetak (online, video dll), sedangkan terbuka berarti tidak ada pembatasan usia, tahun ijazah, masa belajar dan frekuensi mengikuti ujian. Batasan hanya harus sudah menamatkan jenjang pendidikan sekolah menengah atas (SMA) atau sederajat.</p>
					<p> </p>
					<p><strong>Metode Pembelajaran</strong></p>
					<p>Mahasiswa UT diharapkan dapat belajar secara <strong>mandiri</strong>. Cara belajar mandiri menghendaki mahasiswa untuk belajar atas prakarsa atau inisiatif sendiri. Belajar mandiri dapat dilakukan secara sendiri ataupun berkelompok, baik dalam kelompok belajar maupun dalam kelompok tutorial.</p>
					<p> </p>
					<p><strong>Teknis pembelajaran</strong></p>
					<p>Tutorial di UT diselenggarakan dalam 2 model, yaitu:</p>
					<p><span style="text-decoration: underline;">1. Tutorial Online (TUTON)</span></p>
					<p>Tutorial Online atau tutorial elektronik yang dapat diakses melalui menu pada situs UT PUSAT. Tutorial video dengan teknologi streaming video melalui jaringan internet. Melalui layanan ini mahasiswa dapat menyaksikan hasil rekaman tutorial yang dahulu pernah disiarkan melalui TVRI/TPI.</p>
					<p>Jam belajar: Bebas</p>
					<p><span style="text-decoration: underline;">2. Tutorial Tatap Muka (TTM)</span></p>
					<p>TTM dilakukan sebanyak 8 kali, yang terdiri dari 2 jenis;</p>
					<p><em>a) TTM Online (6 kali)</em></p>
					<p>Proses pengajaran akan diajarkan langsung oleh tutornya secara online (teknologi streaming) dengan media Widziq.</p>
					<p>Bagi mahasiswa yang berhalangan hadir pada TTM online, anda dapat mengunduh/menonton stream rekaman kuliah dan juga materi perkuliahan di halaman mata kuliah tersebut. Silahkan masuk ke halaman site jurusan anda (manajemen, inggris, atau komunikasi), kemudian klik mata kuliah tersebut, dan silahkan klik tanggal/bab/topik yang mana anda ingin me-review.</p>
					<p>Jam belajar: Mengikuti jadwal</p>
					<p><em>b) TTM Kelas (2 kali)</em></p>
					<p>Proses pengajaran dilakukan dikelas yang dipandu oleh tutornya secara langsung (face to face).</p>
					<p>Bagi mahasiswa yang berhalangan hadir pada TTM kelas, anda dapat mengunduh/menonton stream rekaman kuliah dan juga materi perkuliahan di halaman mata kuliah tersebut. Silahkan masuk ke halaman site jurusan anda (manajemen, inggris, atau komunikasi), kemudian klik mata kuliah tersebut, dan silahkan klik tanggal/bab/topik yangmana anda ingin me-review.</p>
					<p>Jam belajar: Mengikuti jadwal</p>
					<p>Tempat belajar: Daerah Taipei dapat dilangsungkan di KDEI, dan kelas akan dibuka di kota-kota lain jika cukup banyak mahasiswa yang berdomisili di kota tersebut.</p>
					<p>Komposisi jadwal dan  jumlah kuliah masing-masing TTM Online dan kelas dapat berubah dengan pemberitahuan selanjutnya.</p>
					<p> </p>
					<p><strong>Prosedur Penilaian</strong></p>
					<p>Bagi mata kuliah yg memiliki TTM, maka porsi nilai yang diambil adalah  50% TTM + 50% ujian. Adapun nilai tambahan masih dapat diperoleh apabila mahasiswa tersebut tetap mengikuti TUTON mata kuliah yang di TTM-kan, akan tetapi untuk mengikuti TUTON pada mata kuliah yang memiliki TTM adalah tidak wajib untuk diikuti.</p>
					<p>Bagi matakuliah yang tidak memiliki TTM, hanya TUTON , maka porsi nilainya yang diambil adalah 30%TUTON + 70% ujian. Sedangkan bagi mahasiswa yang tidak mengikuti TTM dan TUTON maka porsi nilainya 100% dari ujian saja.</p>


					
				</article>
				
			</div>
			<!--div class="col-lg-4">

				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
					
					<!-- <div class="widget">
						<h5 class="widgetheading">Kontak</h5>
						Fatya (Taipei) 0984218655<br />
						Nisa (Chungli) 0986481240<br />
						Naufal (Tainan) 097625445<br />
					</div> -->
			<!--/div-->
		</div>
	</div>
</section>
<br>
<br>
