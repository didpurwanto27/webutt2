<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Sejarah</a></h3>
						</div>
					</div>
					
					
					<p>Tenaga Kerja Indonesia (TKI) merupakan salah satu aset bangsa. Tak heran maka mereka sering dijuluki “Pahlawan Devisa” yang mana memberikan kontribusi yang besar untuk bangsa Indonesia. Secara keseluruhan jumlah TKI sendiri adalah 40,26 persen atau 142.983 orang dari total 355.136 pekerja asing di Taiwan (KOMPAS, 2010). Dari jumlah tersebut, ada keinginan dari TKI untuk dapat melanjutkan pendidikan mereka selama bekerja di Taiwan. Hal ini merupakan suatu keinginan positif yang mana setelah mereka selesai dan kembali ke tanah air, mereka memiliki kemampuan (skill) dan ijasah.</p>
					<p> </p>
					<p>Menanggapi hal tersebut Perhimpunan Pelajar Indonesia di Taiwan (PPI Taiwan) bernisiatif untuk memfasilitasi adanya sebuah unit pembelajaran jarak jauh yang dapat menjawab keinginan TKI di Taiwan. Setelah melakukan beberapa pembicaraan informal dengan beberapa organisasi TKI di Taiwan, maka PPI Taiwan melakukan penelitian dan pencarian lembaga apa yang cocok dan sesuai dengan kebutuhan TKI di Taiwan. Pada kesimpulannya, kebanyakan TKI yang ada di Taiwan merupakan lulusan Sekolah Menengah Atas (SMA) atau sederajat dan berkeinginan untuk melanjutkan ke jenjang Universitas. Dalam hal ini PPI Taiwan berkesimpulan bahwa Universitas Terbuka (UT) merupakan lembaga yang dapat menjawab kebutuhan tersebut. UT sendiri merupakan satu-satunya lembaga pendidikan jarak jauh yang didirikan pemerintah dan saat ini sudah memiliki 26 cabang pengelola Luar Negri yang tersebar di seluruh dunia.</p>
					<p> </p>
					<p><strong>Pelaksanaan</strong></p>
					<div style="text-align: justify;"></div>
					<div style="text-align: justify;">PPI Taiwan dalam hal ini sebagai fasilitator untuk terwujudnya UT Luar Negeri di Taiwan memiliki beberapa tahapan diantaranya:</div>
					<div style="text-align: justify;"></div>
					<p>- Tahapan Pertama</p>
					<div style="text-align: justify;">Pada tahap ini akan diadakan survey data TKI di Taiwan yang memiliki keinginan melanjutkan pendidikan ke jenjang Universitas. Data ini akan digunakan sebagai pertimbangan untuk UT pusat dalam upaya membuka cabang UT di Taiwan. Selain itu penjajakan peran KDEI sebagai “center” penyelenggara UT di Taiwan. (hal ini mengacu kepada penyelenggara UT di negara lain yang ditangani oleh KBRI atau Konsulat Jendral RI).</div>
					<div style="text-align: justify;"></div>
					<p>- Tahap Kedua</p>
					<div style="text-align: justify;">Penyusunan Memorandum of Understanding (MoU) antara UT dengan KDEI sebagai penanggung jawab dengan PPI Taiwan sebagai fasilitator. Tercapainya kesepakatan tentang teknis pelaksanaan, biaya yang dibutuhkan mahasiswa dan biaya pihak penyelenggara.</div>
					<div style="text-align: justify;"></div>
					<p>- Tahap Ketiga</p>
					<div style="text-align: justify;">Angkatan pertama UT di Taiwan. Pada tahap ini dibuka pendaftaran untuk penerimaan mahasiswa baru program UT Taiwan. Mahasiswa diharapkan bisa mengikuti modul pembelajaran UT secara online dan belajar secara mandiri. UT Pusat akan mengirimkan utusan penguji setiap akhir semester untuk melakukan evaluasi belajar mahasiswa UT di Taiwan.</div> 

					
				</article>
				
			</div>
			<!--div class="col-lg-4">
				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
			</div-->
		</div>
	</div>
</section>
<br>
<br>